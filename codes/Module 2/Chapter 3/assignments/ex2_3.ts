function checkside()
{
    var x: HTMLInputElement= <HTMLInputElement>document.getElementById("t1");
    var y: HTMLInputElement= <HTMLInputElement>document.getElementById("t2");
    var z: HTMLInputElement= <HTMLInputElement>document.getElementById("t3");

    let l1: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("l1");
    let l2: HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("l2");

    var a: number = +x.value;
    var b: number = +y.value;
    var c: number = +z.value;

    if(a==b && b==c)
    {
        l1.innerHTML = "The Tringle is equilateral";

    }
    else if((a==b && a!=c ) || (b==c && b!=a) || (a==c && c!=b))
    {
        l1.innerHTML = "The Tringle is isosceles";

        if(Math.pow(a,2)==(Math.pow(b,2) + Math.pow(c,2)) ||Math.pow(b,2)==(Math.pow(a,2) + Math.pow(c,2)) 
        ||  Math.pow(c,2)==(Math.pow(a,2) + Math.pow(b,2)))
        {
            l2.innerHTML = "The Tringle is also a Right angled triangle";

        }

    }
    else if(a!=b && b!=c && a!=c)
    {
        l1.innerHTML = "The Tringle is scalene";

        if(Math.pow(a,2)==(Math.pow(b,2) + Math.pow(c,2)) ||Math.pow(b,2)==(Math.pow(a,2) + Math.pow(c,2)) 
        ||  Math.pow(c,2)==(Math.pow(a,2) + Math.pow(b,2)))
        {
            l2.innerHTML = "The Tringle is also a Right angled triangle";

        }

    }
}