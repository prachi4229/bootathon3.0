"use strict";
function checkside() {
    var x = document.getElementById("t1");
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var l1 = document.getElementById("l1");
    var l2 = document.getElementById("l2");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    if (a == b && b == c) {
        l1.innerHTML = "The Tringle is equilateral";
    }
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        l1.innerHTML = "The Tringle is isosceles";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2))
            || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            l2.innerHTML = "The Tringle is also a Right angled triangle";
        }
    }
    else if (a != b && b != c && a != c) {
        l1.innerHTML = "The Tringle is scalene";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2))
            || Math.pow(c, 2) == (Math.pow(a, 2) + Math.pow(b, 2))) {
            l2.innerHTML = "The Tringle is also a Right angled triangle";
        }
    }
}
